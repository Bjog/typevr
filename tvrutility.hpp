#include <opencv2/core/core.hpp>
#include <opencv2/aruco.hpp>
using namespace cv;
using namespace std;

void point3fToMat(Point3f p, Mat m);
vector<vector<Point3f> > createTestBoardPoints(float n, float x, float y); 	
vector<Point3f> getBoardCornerPoints(vector<vector<Point3f> > boardPoints);
vector<Point3f> getBoardInnerCornerPoints(vector<vector<Point3f> > boardPoints);
vector<Point3f> createKeyboardPoints();
void saveJSON(Mat r, Mat t, string filename);
bool readCameraParameters(string filename, Mat &camMatrix, Mat &distCoeffs);


