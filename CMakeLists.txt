cmake_minimum_required(VERSION 3.5)
project(TypewritVR)

find_package( OpenCV REQUIRED)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_BUILD_TYPE Debug)
set(SOURCES main.cpp tvrutility.cpp tvrdrawing.cpp)
add_executable( TypewritVR ${SOURCES})
target_link_libraries( TypewritVR ${OpenCV_LIBS})

