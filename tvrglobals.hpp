#include <opencv2/core/core.hpp>
using namespace cv;
using namespace std;

// double cM[3][3] = { 
// 	{1.4475832982842733*pow(10,3), 0.0,  6.6706203101689596 * pow(10,2)},
// 	{0.0,1.4475832982842733 * pow(10,3), 4.4206911099080077 * pow(10,2)},
// 	{0.0, 0.0, 1.0}};

// Mat cameraMat = Mat(3,3,CV_64F,cM);

// double dV[] = {0.0, 0.0, 0.0, 0.0,-5.9670821218281955 * pow(10,-1) };

// vector<double> distortionVec(dV, dV + sizeof(dV)/sizeof(double));
// Mat distortionMat = Mat(1,5,CV_64F,dV);

float markerSize = 3.7 * pow(10,-3); //converted to metres