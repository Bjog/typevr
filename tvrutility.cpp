#include <opencv2/core/core.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
using namespace cv;
using namespace std;


void point3fToMat(Point3f p, Mat m){
    m.at<double>(0) = p.x;
    m.at<double>(1) = p.y;
    m.at<double>(2) = p.z;
}

vector<vector<Point3f> > createTestBoardPoints(float len, float x, float y){
	float xh = x/2;
	float yh = y/2;
	//Top Left-  ID0
	vector<Point3f> marker0 { {0-xh,0-yh,0},{len-xh,0-yh,0},  {len-xh,len-yh,0},    {0-xh,len-yh,0} };
	//Top Right- ID1
	vector<Point3f> marker1 { {x-xh,0-yh,0},{len+x-xh,0-yh,0},{len+x-xh,len-yh,0},  {x-xh,len-yh,0}};
	//Bot Left-  ID2
	vector<Point3f> marker2 { {0-xh,y-yh,0},{len-xh,y-yh,0},  {len-xh,y+len-yh,0},  {0-xh,len+y-yh,0}};
	//Bot Right- ID3
	vector<Point3f> marker3 { {x-xh,y-yh,0},{len+x-xh,y-yh,0},{len+x-xh,y+len-yh,0},{x-xh,len+y-yh,0}};

	vector<vector<Point3f> > markers {marker0,marker1,marker2,marker3};

	#ifdef __DEBUG__
	for(int i = 0; i<markers.size(); i++){
			cout << endl;
		for(int j = 0; j<markers[i].size(); j++){
				cout << markers[i][j] << endl;
		}
	}
	#endif

	return markers;
}

//Gets top-left corner of each marker
vector<Point3f> getBoardCornerPoints(vector<vector<Point3f> > boardPoints){
	vector<Point3f> marker0 = boardPoints[0];
	vector<Point3f> marker1 = boardPoints[1];
	vector<Point3f> marker2 = boardPoints[2];
	vector<Point3f> marker3 = boardPoints[3];

	Point3f p0(marker0[0].x,marker0[0].y,0.0);
	Point3f p1(marker1[0].x,marker1[0].y,0.0);
	Point3f p2(marker2[0].x,marker2[0].y,0.0);
	Point3f p3(marker3[0].x,marker3[0].y,0.0);

	vector<Point3f> boundsVector {p0,p1,p2,p3};

	return boundsVector;

}

//Gets inner corner of each marker - handtracking space
vector<Point3f> getBoardInnerCornerPoints(vector<vector<Point3f> > boardPoints){
	vector<Point3f> marker0 = boardPoints[0];
	vector<Point3f> marker1 = boardPoints[1];
	vector<Point3f> marker2 = boardPoints[2];
	vector<Point3f> marker3 = boardPoints[3];

	Point3f p0(marker0[3].x,marker0[3].y,0.0);
	Point3f p1(marker1[2].x,marker1[2].y,0.0);
	Point3f p2(marker2[0].x,marker2[0].y,0.0);
	Point3f p3(marker3[1].x,marker3[1].y,0.0);

	vector<Point3f> boundsVector {p0,p1,p2,p3};

	return boundsVector;

}

vector<Point3f> createKeyboardPoints(){
	//Based on the Logitech K360. All lengths in mm.
	float x,y,z;
	x = 184.0;
	y = 400.0;
	z = 32.0;

	float xoff,yoff,zoff; //offsets
	xoff = -x/2;
	yoff = -y/2;
	zoff = 0;

	vector<Point3f> v;
	//Generate from bottom-left, clockwise, edge pairs.
	
	//Lower Rectangle
	v.push_back(Point3f(0 + xoff,0 + yoff,0 + zoff));
	v.push_back(Point3f(0 + xoff,y + yoff,0 + zoff));

	v.push_back(Point3f(0 + xoff,y + yoff,0 + zoff));
	v.push_back(Point3f(x + xoff,y + yoff,0 + zoff));

	v.push_back(Point3f(x + xoff,y + yoff,0 + zoff));
	v.push_back(Point3f(x + xoff,0 + yoff,0 + zoff));

	v.push_back(Point3f(x + xoff,0 + yoff,0 + zoff));
	v.push_back(Point3f(0 + xoff,0 + yoff,0 + zoff));

	//Upper Rectangle
	v.push_back(Point3f(0 + xoff,0 + yoff,-z + zoff));
	v.push_back(Point3f(0 + xoff,y + yoff,-z + zoff));

	v.push_back(Point3f(0 + xoff,y + yoff,-z + zoff));
	v.push_back(Point3f(x + xoff,y + yoff,-z + zoff));

	v.push_back(Point3f(x + xoff,y + yoff,-z + zoff));
	v.push_back(Point3f(x + xoff,0 + yoff,-z + zoff));

	v.push_back(Point3f(x + xoff,0 + yoff,-z + zoff));
	v.push_back(Point3f(0 + xoff,0 + yoff,-z + zoff));

	//Corners
	v.push_back(Point3f(0 + xoff,0 + yoff,0 + zoff));
	v.push_back(Point3f(0 + xoff,0 + yoff,-z + zoff));


	v.push_back(Point3f(0 + xoff,y + yoff,0 + zoff));
	v.push_back(Point3f(0 + xoff,y + yoff,-z + zoff));

	v.push_back(Point3f(x + xoff,y + yoff,0 + zoff));
	v.push_back(Point3f(x + xoff,y + yoff,-z + zoff));

	v.push_back(Point3f(x + xoff,0 + yoff,0 + zoff));
	v.push_back(Point3f(x + xoff,0 + yoff,-z + zoff));

	return v;

}

void saveJSON(Mat r, Mat t, string filename){
	FileStorage fs("pose.json",FileStorage::WRITE);
	fs << "r" << r;
	fs << "t" << t;
	fs.release();
}

bool readCameraParameters(string filename, Mat &camMatrix, Mat &distCoeffs) {
	cout << "Reading Camera Parameters from " << filename << endl;
    FileStorage fs(filename, FileStorage::READ);
    if(!fs.isOpened()){
    	cout << "File could not be opened " << filename << endl;
        return false;
    }

    fs["camera_matrix"] >> camMatrix;
    fs["distortion_coefficients"] >> distCoeffs;
    cout << "File opened and read" << endl;
    return true;
}
