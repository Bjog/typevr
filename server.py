from flask import Flask, request, send_file
import json
app = Flask(__name__)


@app.route("/pose/", methods = ['POST','GET'])
def handle_pose():
	if(request.method == "POST"):
		#Inbound data
		f = open("pose.data","w")
		data = request.get_json()
		dataJSON = json.dumps(strip_data(data))
		f.write(dataJSON)
		f.close()
		return "POST received"
	else:
		#Outbound data
		f = open("pose.json","r")
		posedata = f.read()
		pose = json.dumps(strip_data(json.loads(posedata)))
		f.close()
		return pose

@app.route("/texture/",methods=['GET'])
def handle_texture():
	if(request.method=="GET"):
		return send_file("texture.jpg",mimetype="image/jpeg")

#Extracts only R and T vector data fields from JSON String
def strip_data(s):
	data = {}
	data["r"] = s["r"]["data"]
	data["t"] = s["t"]["data"]
	return data


if __name__ == '__main__':
	app.debug = True
	app.run("127.0.0.1",5603)

