#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <string>
#include <list>
#include <time.h>

#ifndef _TVRGLOBALS_H_
#define _TVRGLOBALS_H_
#include "tvrglobals.hpp"
#endif

#ifndef _TVRUTILITY_H_
#define _TVRUTILITY_H_
#include "tvrutility.hpp"
#endif

#ifndef _TVRDRAWING_H_
#define _TVRDRAWING_H_
#include "tvrdrawing.hpp"
#endif

using namespace std;
using namespace cv;

void perspWarp();

int main(int argc, char *argv[]){
	VideoCapture cap(0);
	cap.set(CAP_PROP_FRAME_HEIGHT,480);
	cap.set(CAP_PROP_FRAME_WIDTH,640);
	cap.set(CAP_PROP_FPS,60);
	Mat frame, outframe; //Frame from camera

	vector<int> ids;
	vector< vector<Point2f> > corners;

	Ptr<aruco::Dictionary> dict = aruco::getPredefinedDictionary(aruco::DICT_6X6_50);
	
	Mat rvecs,tvecs, avgrvec, avgtvec, cumrvec, cumtvec;
	list<Mat> rlist, tlist;


	//Create Aruco Board
	vector<int> idPriori {0,1,2,3};

	vector<vector<Point3f> > objPoints;
	int poseBufferSize = 2;

	if(argc == 5){
		poseBufferSize = atoi(argv[4]);
	}


	if(argc < 4){
		objPoints = createTestBoardPoints(37,83,395);
		cout << "Using default keyboard configuration" << endl;
		cout << "Marker Size = 37mm \nKBD Height = 83mm \nKBD Width = 395mm" << endl;
	} else {


		int bMarkerSize = atoi(argv[1]);
		int bHeight = atoi(argv[2]);
		int bWidth = atoi(argv[3]);

		objPoints = createTestBoardPoints(bMarkerSize,bHeight,bWidth);

		cout << "Marker Size = " << bMarkerSize << "\nKBD Height = " << bHeight << "\nKBD Width = " << bWidth << endl; 

	}

	cout << "Buffer Size: " << poseBufferSize << endl;
	
	Ptr<aruco::Board> board = aruco::Board::create(objPoints,dict,idPriori);
	
	vector<Point3f> boardBounds = getBoardInnerCornerPoints(objPoints);
	vector<Point3f> keyboardPoints = createKeyboardPoints();
	vector<Point2f> imgPoints; //Projected keyboard image
	vector<Point2f> imgCornerPoints; //Bounding area for hand tracking

	Mat cameraMat, distortionMat;
	readCameraParameters("E:\\Documents\\typewritvr\\typewritvr\\configs\\logitech_c930e_calibration.yml",cameraMat,distortionMat);
	
	int valid = 0;	
	int warpvalid = 0;

	float warpWidth = 480/2;
	float warpHeight= 640/2;
	Mat warpMat, warpFrame;

	vector<Point2f> warpBounds{ {0.0,0.0},{warpWidth,0.0},{0.0,warpHeight},{warpWidth,warpHeight}};

	namedWindow("Camera Stream",WINDOW_NORMAL);
	namedWindow("Warped Image", 1);



	while(true){


		cap >> frame;
		frame.copyTo(outframe);

		aruco::detectMarkers(frame,dict,corners,ids);
		if(ids.size() > 0){
			aruco::drawDetectedMarkers(outframe,corners,ids);
		}

		if(ids.size() > 0){
			valid = aruco::estimatePoseBoard(corners,ids,board,cameraMat,distortionMat,rvecs,tvecs);

			if(valid > 0){

				if(rlist.size() >= poseBufferSize){
					rlist.pop_front();
					tlist.pop_front();
				}

				rlist.push_back(rvecs.clone());
				tlist.push_back(tvecs.clone());

				cumrvec = Mat::zeros(rvecs.rows,rvecs.cols,rvecs.type());
				cumtvec = Mat::zeros(tvecs.rows,tvecs.cols,tvecs.type());

				for (list<Mat>::iterator it = rlist.begin(); it != rlist.end(); ++it){
					accumulate(*it,cumrvec);
				}


				for (list<Mat>::iterator it = tlist.begin(); it != tlist.end(); ++it){
					accumulate(*it,cumtvec);
				}

				Mat avgrvec = cumrvec / (double) rlist.size();
				Mat avgtvec = cumtvec / (double) tlist.size();

				//Save r and t as JSON.
				saveJSON(avgrvec, avgtvec, "pose.json");

				aruco::drawAxis(outframe,cameraMat,distortionMat,avgrvec,avgtvec,100);

				//For wireframe model of keyboard
				projectPoints(keyboardPoints,avgrvec,avgtvec,cameraMat,distortionMat,imgPoints);
				drawWireframe(imgPoints,outframe, Scalar(255,0,255));

				//For hand-tracking area
				projectPoints(boardBounds,avgrvec,avgtvec,cameraMat,distortionMat,imgCornerPoints);
				drawCrosshair(imgCornerPoints,25,outframe);

				//Perspective Warp
				warpMat = getPerspectiveTransform(imgCornerPoints,warpBounds);
				warpPerspective(frame,warpFrame,warpMat,Size(warpWidth,warpHeight));


			}
		} else {
			drawWireframe(imgPoints,outframe, Scalar(255,255,255));
		}

		imshow("Camera Stream",outframe);
		if(valid){
			imwrite("texture.jpg", warpFrame);
			imshow("Warped Image", warpFrame);
		}
		if(waitKey(30)>=0){
			cout << "Terminated by user." << endl;
			break;
		}
    }

	cap.release();
	return 0;
}

void perspWarp(){

}
