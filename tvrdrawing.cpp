#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>

using namespace cv;
using namespace std;

#define WIDTH 640
#define HEIGHT 480

bool withinBounds(Point2f p);
bool withinBounds(Point3f p);
bool withinBounds(vector<Point2f> points);
bool withinBounds(vector<Point3f> points);

class Edge{
	private:
		Point3f pt1;
		Point3f pt2;

	public:
		Edge(Point3f ,Point3f );
		vector<Point3f>getPoints();
		void print() const;
};


Edge::Edge(Point3f a, Point3f b){
	pt1 = a;
	pt2 = b;
};

vector<Point3f> Edge::getPoints(){
	vector<Point3f> v;

	v.push_back(this->pt1);
	v.push_back(this->pt2);

	return v;
}


void Edge::print() const{
	cout << pt1 << endl;
	cout << pt2 << endl;
};

//p must be pair-wise edges. I.e. p[0] and p[1] is one edge, p[2] and p[3] is next edge, etc.
void drawWireframe(vector<Point2f> p, Mat image, Scalar color){
	if(p.size() % 2 == 0 && p.size() > 0){
		for(int i = 0; i < p.size(); i+=2){
			line(image,p[i],p[i+1], color,2);
		}
	}
}

void drawCrosshair(Point2f p, int length, Mat frame){
	if(withinBounds(p)){
		Point2f lH(length,0.0);
		Point2f lV(0.0,length);
		line(frame,p - lH, p+lH,Scalar(0,255,255),2);
		line(frame,p - lV, p+lV,Scalar(0,255,255),2);
	}
}

void drawCrosshair(vector<Point2f> p, int length, Mat frame){
	for(int i = 0; i < p.size();i++){
		drawCrosshair(p[i],length,frame);
	}
}

void drawBoardBounds(vector<Point2f> boardPoints, Mat image){
	//All points within bounds
	Scalar color(255,0,255);
	line(image,boardPoints[0],boardPoints[1],color,2);
	line(image,boardPoints[1],boardPoints[3],color,2);
	line(image,boardPoints[3],boardPoints[2],color,2);
	line(image,boardPoints[2],boardPoints[0],color,2);		
}

bool withinBounds(Point2f p){
	if( (p.x < WIDTH) && (p.y < HEIGHT) ){
		return true;
	} else {
		return false;
	}
}

bool withinBounds(Point3f p){
	if( (p.x < WIDTH) && (p.y < HEIGHT) ){
		return true;
	} else {
		return false;
	}
}

bool withinBounds(vector<Point3f> points){
	for(int i = 0; i < points.size();i++){
		if(!withinBounds(points[i])){
			return false;
		}
	}
	return true;
}

bool withinBounds(vector<Point2f> points){
	for(int i = 0; i < points.size();i++){
		if(!withinBounds(points[i])){
			return false;
		}
	}
	return true;
}

