#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <iostream>

using namespace cv;
using namespace std;

class Edge{
    private:
        Point3f pt1;
        Point3f pt2;

	public:
		Edge(  Point3f, Point3f);
        vector<Point3f> getPoints();
        void print() const;
};

void drawBoardBounds(vector<Point2f> boardPoints, Mat image);
void drawCrosshair(Point2f p, int length, Mat frame);
void drawCrosshair(vector<Point2f> p, int length, Mat frame);
bool withinBounds(Point2f p);
bool withinBounds(Point3f p);
bool withinBounds(vector<Point2f> points);
bool withinBounds(vector<Point3f> points);
void drawWireframe(vector<Point2f> p, Mat image,Scalar color);
